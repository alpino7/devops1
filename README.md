1. https://www.virtualbox.org/wiki/Downloads üzerinden virtualbox'ı indirip bilgisayarıma kurdum.
2. http://ftp.itu.edu.tr/Mirror/CentOS/8.3.2011/isos/x86_64/ üzerinden CentOS isosunu indirdim.
3. virtualbox'ı çalıştırıp ISO vasıtasıyla CentOS'u kurdum. Kurulumu yapmadan önce virtualbox menüsünden 10GB kapasiteyle 2. diski ekledim.
4. Öncelikle su komutuyla root access edindim. fdisk /dev/sdb ve ardından d komutuyla kurulumda eklediğim harddiski sıfırladım. 
5. mkfs.ext3 /dev/sdb komutuyla diski kullanılabilir hale getirdim. 
6. mkdir /bootcamp ve mount /dev/sdb /bootcamp komutlarıyla diski bootcampe mountladım. vi /etc/fstab ile Kurulumu tamamladım ve e2label /dev/sdb /bootcamp ile diske bootcamp adını verdim.
7. cd /opt komutuyla opt klasörüne geldim ve mkdir bootcamp komutuyla bootcamp adlı bir klasör oluşturdum.
8. cd /opt/bootcamp komutuyla txt oluşturmak istediğim klasöre geldim ve cat > bootcamp.txt komutuyla istenilen txt dosyasını oluşturdum.
9. echo "merhaba trendyol" > bootcamp.txt komutuyla txt üzerine yazdım.
10. Son olarak find /opt/bootcamp -name '*bootcamp.txt*' -exec mv -t /bootcamp {} + komutuyla tek komut kullanarak txt dosyasını bootcamp diskine taşıdım.




